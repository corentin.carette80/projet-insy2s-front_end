import React from "react";
import useBuyHook from "./useBuyHook";
import { BsFillCartFill } from "react-icons/bs";

const Buy = (props) => {
  const { addStockage } = useBuyHook();
  return (
    <>
      <button
        className="pan_product"
        onClick={() => {
          addStockage(props);
        }}
      >
        Ajouter au panier 
        {/* <BsFillCartFill /> */}
      </button>
    </>
  );
};

export default Buy;
