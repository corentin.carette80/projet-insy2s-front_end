import React from "react";
import { useState, useEffect } from "react";

const useBuyHook = () => {
  const [isStoring, setIsStoring] = useState();
  const [listOfShop, setListOfShop] = useState();
  const [exist, setExist] = useState("false");

  const addStockage = (product) => {
    let newProduct;
    let shoppingCart = [];
    shoppingCart = JSON.parse(localStorage.getItem("Panier")) || [];
    setIsStoring(false);
    // Condition if product is already in the shoppingCart
    console.log(shoppingCart);
    if (
      shoppingCart.some(
        (element) => element.product._id === product.product._id
      )
    ) {
      // CartItem ==> item of the shoppingCart
      let cartItem = shoppingCart.find(
        (element) => element.product._id == product.product._id
      );
      console.log("j'existe deja", cartItem.quantity);
      setExist("true");
      cartItem.quantity = cartItem.quantity + 1;
    } else {
      setExist("false");
      newProduct = { product: product.product, quantity: 1 };
    }

    if (shoppingCart.length === 0) {
      shoppingCart.push({
        product: product.product,
        quantity: 1,
      });
      localStorage.setItem("Panier", JSON.stringify(shoppingCart));
      setExist("true");
    } else if (exist === "false") {
      shoppingCart.push(newProduct);
      setExist("true");
      localStorage.setItem("Panier", JSON.stringify(shoppingCart));
    }
    setListOfShop(shoppingCart);
    setIsStoring(true);
  };

  useEffect(() => {
    if (localStorage.getItem("Panier") === null) {
      localStorage.setItem("Panier", JSON.stringify([]));
    }
    if (isStoring == true) {
      localStorage.setItem("Panier", JSON.stringify(listOfShop));
      setIsStoring(false);
    }
  }, [isStoring]);

  const suppStockage = (product) => {
    let settings = JSON.parse(localStorage.getItem("Panier"));

    let found = settings.find((x) => x === props.product._id);
    settings.splice(settings.indexOf(found), 1);
    setListOfShop(localStorage.getItem("Panier"));
    localStorage.setItem("Panier", JSON.stringify(settings));
  };

  return { addStockage, suppStockage };
};

export default useBuyHook;
