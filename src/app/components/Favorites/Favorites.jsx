import React from "react";
import useFavoritesHook from "./useFavoritesHook";
import favoris from "../../assets/images/favoris.png";
import favoris2 from "../../assets/images/favoris2.png";

const Favorites = (props) => {
  const { addStockage, suppStockage } = useFavoritesHook(props);
  return (
    <div>
      <div className="filmtitle m-6">
        {!localStorage.getItem("Favorites")?.includes(props.id) ? (
          <div
            onClick={() => {
              addStockage(props.product);
            }}
          >
            <img className="favoris-icon" src={favoris} alt="" />
          </div>
        ) : (
          <div
            onClick={() => {
              suppStockage(props.product);
            }}
          >
            <img className="favoris-icon" src={favoris2} alt="" />
          </div>
        )}
      </div>
    </div>
  );
};

export default Favorites;
