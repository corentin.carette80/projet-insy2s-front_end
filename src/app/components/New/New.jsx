import React from "react";
import Promo from "../accueil/promo";
import AdvantageUser from "../accueil/advantage";
import BreadCrumb from "../breadCrumb/BreadCrumb";
import useTopSalesHook from "../topSales/useTopSalesHook";
import ListOfProducts from "../product/ListOfProducts";
import Pagination from "../pagination/Pagination";
import Aside from "../sidebar/AsideComponent";
import NewGlob from "./NewGlob";
import Section from "../genericComponents/Section";
import("../../assets/styles/NewView.scss");
const New = () => {
  const {
    filterOn,
    filteredData,
    products,
    filters,
    filterProducts,
    paginatedData,
    productsToList,
    pages,
  } = useTopSalesHook();
  return (
    <>
      <div className="newcontainer">
        <Section section={"Nouveauté"} />
        <NewGlob />
        <Aside parentFilter={filterProducts} />
        
        <ListOfProducts
          toLoop={paginatedData}
          products={paginatedData}
          noItems={paginatedData.length == 0 ? true : false}
        />
        {products.length ? (
          <Pagination
            parentCallback={productsToList}
            products={filterOn ? filteredData : products}
            resetCurrentPage={filterOn ? true : false}
            nofilter={products.length == 0 ? true : false}
            filterOn={filterOn}
            reverse={true}
            pages={pages}
          />
        ) : (
          <span>loading ..</span>
        )}
      </div>
      <Promo />
      <AdvantageUser />
    </>
  );
};

export default New;
