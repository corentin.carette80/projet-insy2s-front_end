import React from "react";
import ResetPassword from "../../views/ResetPassword";
export default function Modal() {
  const [showModal, setShowModal] = React.useState(false);
  return (
    <>
      <button
        type="button"
        className="button_contact"
        onClick={() => setShowModal(true)}
      >
        Réinitialiser
      </button>
      {showModal ? (
        <>
          <div className="modal">
            <div className="relative w-auto  my-12 mx-auto ">
              <div className="">
              <div>
                    <svg
                      onClick={() => setShowModal(false)}
                      version="1.1"
                      className="remove_rein"
                      x="0px"
                      y="0px"
                      viewBox="0 0 60 60"
                      enableBackground="new 0 0 60 60"
                    >
                      <polygon points="38.936,23.561 36.814,21.439 30.562,27.691 24.311,21.439 22.189,23.561 28.441,29.812 22.189,36.064 24.311,38.186 30.562,31.934 36.814,38.186 38.936,36.064 32.684,29.812" />
                    </svg>
                  </div>
                
                 
                <div className="bobyModal ">
                  <ResetPassword />
                </div>
              </div>
            </div>
          </div>
        </>
      ) : null}
    </>
  );
}
