import React from 'react';
import {Formik,Form,Field,ErrorMessage } from "formik";
import { Link } from 'react-router-dom';
import { URL_RESET_ENVOYE } from '../../shared/constants/urls/urlConstants';
import useResetpasswordHook from './useResetpasswordHook';

const ResetPassword =(props)=> {
    const {sendmail,mailSchema,mailValues} = useResetpasswordHook(props);
  return <>
            <Formik validationSchema={mailSchema} 
            initialValues={mailValues} 
            onSubmit={values => {
                console.log(values);
          }}>
                <Form>
            <div >
            <h1>Reinitialiser votre  mot de passe</h1>
            <Field name="mail" type="mail" placeholder="Veuillez entrer votre email svp " className="inputReset"  />
            <span>  <ErrorMessage name='mail'/> </span>
            <button onClick={sendmail}  type="submit"  ><Link to={URL_RESET_ENVOYE} className="button_contact">envoyer</Link></button>
        </div>
        </Form>
        </Formik>
        </>
}

export default ResetPassword;
