import React from "react";

import { Link, Navigate } from "react-router-dom";
import { Formik, Form } from "formik";
import { TextField } from "./TextField";
import * as Yup from "yup";
import axios from "axios";
import "../../assets/styles/form.scss";
import { URL_LOGIN } from "../../shared/constants/urls/urlConstants";
import { useState } from "react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "../../assets/styles/signup.scss";
import Section from "../genericComponents/Section";
import authService from "../../services/auth.service";
export default function SignUp(props) {
  const [message, setMessage] = useState("");
  const [successful, setSuccessful] = useState(false);
  const [convertisseur, setConvertisseur] = useState(false);
  const changeInput = (e) => {
    setConvertisseur(!convertisseur);
  };
  const validate = Yup.object({
    firstName: Yup.string()
      .max(15, "Doit contenir moins de 15 lettres")
      .required("Requis"),
    lastName: Yup.string()
      .max(20, "Doit contenir moins de 20 lettres")
      .required("Requis"),
    email: Yup.string().email("L'adresse n'est pas valide").required("Requis"),
    password: Yup.string()
      .min(6, "Doit contenir au moins 6 lettres")
      .required("Requis"),
    confirmPassword: Yup.string()
      .oneOf([Yup.ref("password"), null], "Les mots doivet être identiques")
      .required("Confirmer le mot de passe"),
  });
  //url requete bdd
  const urlRegister = "http://localhost:5000/api/users/register";
  //requete vers le back
  const onSubmit = (values) => {
    const data = {
      firstName: values.firstName,
      lastName: values.lastName,
      email: values.email,
      password: values.password,
    };
    authService
      .register(
        values.firstName,
        values.lastName,
        values.email,
        values.password
      )
      .then(
        () => {
          toast.success("😀 Merci de vous êtes inscrit !...", {
            position: "top-center",
            autoClose: 2500,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          (response) => {
            setMessage(response.data.message);
            setSuccessful(true);
          };
        },
        (error) => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();
          setMessage(resMessage);
          setSuccessful(false);
        }
      );
  };
  return (
    <div className="signup3_container">
      <Section section={"CRÉER UN COMPTE"} />
      {message && (
        <div className="form-group">
          <div
            className={
              successful ? "alert alert-success" : "alert alert-danger"
            }
            role="alert"
          >
            {message}
          </div>
        </div>
      )}
      <Formik
        initialValues={{
          firstName: "",
          lastName: "",
          email: "",
          password: "",
          confirmPassword: "",
        }}
        onSubmit={onSubmit}
        validationSchema={validate}
      >
        {(formik) => (
          <Form>
            <label className="label_sign">
              <span>Prénom</span>
              <TextField name="firstName" type="text" className="input_sign" />
            </label>
            <label className="label_sign">
              <span>Nom</span>
              <TextField name="lastName" type="text" className="input_sign" />
            </label>
            <label className="label_sign">
              <span>Email</span>
              <TextField name="email" type="email" className="input_sign" />
            </label>
            <label className="label_sign">
              <span>Mot de passe</span>
              <TextField
                name="password"
                type={convertisseur ? "text" : "password"}
                className="input_sign"
              />
            </label>
            <label className="invi_inf">
              <input
                className="invi_sign"
                type="checkbox"
                defaultValue="text"
                onClick={changeInput}
              />
              <span>
                <i className="fa fa-search see_search_sign"></i>
              </span>
            </label>
            <label className="label_sign">
              <span>Confirmer le mot de passe</span>
              <TextField
                name="confirmPassword"
                type="password"
                className="input_sign"
              />
            </label>
            <fieldset>
              <div className="up_sign">
                <label>
                  <input
                    type="checkbox"
                    name="interest"
                    value="coding"
                    required
                  />
                  Sauvegarder mes coordonnées
                </label>
              </div>
            </fieldset>
            <div>
              <button type="submit">S'inscrire</button>
              <p className="up_sign">
                J'ai déjà un compte ?<Link to={URL_LOGIN}>Je me connecte</Link>
              </p>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
}
