import React from "react";
import { ErrorMessage, useField } from "formik";
import "../../assets/styles/index.scss";

export const TextField = ({ label, ...props }) => {
  const [field, meta] = useField(props);
  // console.log(field, meta)
  return (
    <div className="formContain">
      <div className="form">
        <input
          placeholder={label}
          className={` ${meta.touched && meta.error && "is-invalid"}`}
          {...field}
          {...props}
          autoComplete="off"
        />
      </div>
      <span className="care_sign">
        <ErrorMessage name={field.name} />
      </span>
    </div>
  );
};
