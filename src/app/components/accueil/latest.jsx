import React from "react";
import News from "../../assets/images/news.png";
import Movie from "../../assets/images/movie.jpg";
import { Link } from "react-router-dom";
import Top from "../../assets/images/top.jpg";
import {
  URL_NAV_CARD,
  URL_NAV_NEW,
  URL_NAV_TOP,
  URL_NAV_SHOP,
  URL_HOME,
  URL_SEARCH_PAGE,
  URL_RAYONS,
} from "../../shared/constants/urls/urlConstants";
import Button from "../genericComponents/Button";
export default function Latest() {
  return (
    <div className="latest">
      <div className="latest-item">
        <div className="latest-item-overlay">
          <Link to={URL_RAYONS} className="btn-overlay">
            <div className="text">
              <h2>Rayons</h2>
              <p>L'ensemble de nos produits rien que pour vous ..</p>
            </div>
            <div className="overlay"></div>
            <img src={News} className="latest-picture" />
          </Link>
        </div>
        <Button url={URL_RAYONS} link={"rayons"}></Button>
      </div>
      <div className="latest-item">
        <div className="latest-item-overlay">
          <Link to={URL_NAV_NEW} className="btn-overlay">
            <div className="text">
              <h2>Dernières nouveautés</h2>
              <p>Toutes nos nouveautés rien que pour vous ..</p>
            </div>
            <div className="overlay"></div>
            <img src={Movie} className="latest-picture" />
          </Link>
        </div>
        <Button url={URL_NAV_NEW} link={"Nouveautés"}></Button>
      </div>
      <div className="latest-item">
        <div className="latest-item-overlay">
          <Link to={URL_NAV_TOP} className="btn-overlay">
            <div className="text">
              <h2>Top Ventes</h2>
              <p>
                La sélection de nos produits les plus recherchés rien que pour
                vous ..
              </p>
            </div>
            <div className="overlay"></div>
            <img src={Top} className="latest-picture" />
          </Link>
        </div>
        <Button url={URL_NAV_TOP} link={"Top Ventes"}></Button>
      </div>
    </div>
  );
}
