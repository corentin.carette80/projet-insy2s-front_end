import React from "react";
import Squid from "../../assets/images/Squid.png";
import Lagertha from "../../assets/images/lagertha.png";
import Pull from "../../assets/images/pull.png";
import Shirt from "../../assets/images/shirt.png";
import Section from "../genericComponents/Section";
import ProductSheet from "../genericComponents/ProductSheet";

export default function Promo() {
  return (
    <div className="backgrpromo">
      <Section section={"Nos promotions"} />
      <div className="promo">
        <ProductSheet image={Squid} title={"Mug Squid Game"} price={"90"} />
        <ProductSheet
          image={Lagertha}
          title={"Figurine Lagertha"}
          price={"35"}
        />
        <ProductSheet image={Pull} title={"Pull 001"} price={"70"} />
        <ProductSheet
          image={Shirt}
          title={"T-Shirt Captain America"}
          price={"55"}
        />
      </div>
    </div>
  );
}
