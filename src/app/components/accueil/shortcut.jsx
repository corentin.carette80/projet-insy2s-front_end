import React from "react";
import Accessories from "../../assets/images/accessory.png";
import GOT from "../../assets/images/got.jpg";
import HP from "../../assets/images/HP.jpg";
import { useContext } from "react";
import { DarkThemeContext } from "../../context/DarkThemeContext";
import Button from "../genericComponents/Button";
import {
  URL_NAV_NEW,
  URL_NAV_TOP,
} from "../../shared/constants/urls/urlConstants";

export default function Shortcut() {
  const { toggleDarkTheme, darkTheme } = useContext(DarkThemeContext);
  return (
    <div className="shortcut-section">
      <h3 className="shortcut-title">Notre Sélection</h3>
      <hr className="section-line" />
      <div className="shortcut">
        <div className="shortcut-item">
          <img src={Accessories} className="shortcut-img" />
          <Button url={URL_NAV_NEW} link={"Accessoires"}></Button>
        </div>
        <div className="shortcut-item">
          <img src={GOT} className="shortcut-img" />
          <Button url={URL_NAV_TOP} link={"Top séries de la semaine"}></Button>
        </div>
        <div className="shortcut-item">
          <img src={HP} className="shortcut-img" />
          <Button url={URL_NAV_NEW} link={"Jeux et cadeaux"}></Button>
        </div>
      </div>
    </div>
  );
}

// fdsfdsfdsfdf
