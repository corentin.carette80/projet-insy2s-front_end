import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import PreviewImage from "./PreviewImage";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { URL_ADMIN_HOME } from "../../shared/constants/urls/urlConstants";
import NavAdmin from "./NavAdmin";
import useAddProductHook from "./useAddProductHook";
import axios from "axios";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useNavigate } from "react-router-dom";
import "./star.css";
import Section from "../genericComponents/Section";

const AddProduct = (props) => {
  const [isStar, setStar] = useState("false");
  const {
    categories,
    selected,
    subCategories,
    d,
    options,
    validate,
    changeSelectOption,
  } = useAddProductHook(props);
  let navigate = useNavigate();
  const [selection, setSelection] = useState(false);
  const handleToggle = () => {
    setStar(!isStar);
  };
  function CheckBox(props) {
    return (
      <Field name={props.name}>
        {({ field, form }) => (
          <label>
            <input
              {...field}
              type="checkbox"
              className="hidden"
              onClick={handleToggle}
            />
            <span className={isStar ? "unstarred" : "starred"}>
              <span className="star-icon fa fa-star"></span>
            </span>
          </label>
        )}
      </Field>
    );
  }
  return (
    <>
        <NavAdmin />
        <div className="bg-grey-custom w-10/12 mx-auto mb-10 ">
          <Formik
            initialValues={{
              selection: false,
              name: "",
              price: "",
              licence: "",
              stock: "",
              category: "",
              subcategory: "",
              image: null,
              caracteristics: "",
              description: "",
              online: true,
            }}
            validationSchema={validate}
            onSubmit={async (values) => {
              const { initialValues, ...data } = values;
              const formData = new FormData();
              if (values.selection === 0) {
                formData.append("selection", false);
              } else {
                formData.append("selection", true);
              }
              formData.append("name", values.name);
              formData.append("price", values.price);
              formData.append("licence", values.licence);
              formData.append("stock", values.stock);
              formData.append("categoryId", [values.category]);
              formData.append("subCategoryId", [values.subcategory]);
              formData.append("image", values.image);
              formData.append("feature", values.caracteristics);
              formData.append("description", values.description);
              formData.append("online", true);
              console.log(data);
              console.log(values.image.name);

              const response = await axios
                .post("http://localhost:5000/api/products/", formData)
                .catch((err) => {
                  if (err && err.response) console.log("Error: ", err);
                })
                .then(() => {
                  toast.success("Produit ajouté avec succès !", {
                    position: "top-center",
                    autoClose: 2500,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                  });
                  navigate("/admin");
                });
            }}
          >
            {({ values, isSubmitting, setFieldValue }) => (
              <div className="addproduct_container">
                <Section section={"AJOUTER UN PRODUIT"} />
                <Form
                  className="esp_information"
                  encType="multipart/form-data"
                >
                    {/* <label className="flex items-center font-firaCode">
                      Produit de Sélection
                    </label>
                      <CheckBox name="selection" value={!selection} /> */}



                    <label className="">
                      Nomination
                    </label>
                      <Field
                        type="text"
                        id="name"
                        name="name"
                        className="border-none w-80"
                      />
                      <ErrorMessage
                        name="name"
                        component="small"
                        className=""
                      />
                    <label className="">
                      Prix
                    </label>
                      <Field
                        type="number"
                        id="price"
                        name="price"
                        className="border-none w-80"
                      />
                      <ErrorMessage
                        name="price"
                        component="small"
                        className=""
                      />
                    <label className="">
                      Licence
                    </label>
                      <Field
                        type="text"
                        id="licence"
                        name="licence"
                        className=""
                      />
                      <ErrorMessage
                        name="licence"
                        component="small"
                        className=""
                      />
                    <label className="">
                      Caractéristique
                    </label>
                      <Field
                        type="text"
                        id="caracteristics"
                        name="caracteristics"
                        className=""
                      />
                      <ErrorMessage
                        name="caracteristics"
                        component="small"
                        className=""
                      />
                    <label className="">
                      Quantité
                    </label>
                      <Field
                        type="number"
                        id="stock"
                        name="stock"
                        className=""
                      />
                      <ErrorMessage
                        name="stock"
                        component="small"
                        className=""
                      />
                    <label className="">
                      Catégories
                    </label>
                      <Field
                        as="select"
                        name="category"
                        id="category"
                        className=""
                        onChange={(e) => {
                          changeSelectOption(e, setFieldValue);
                        }}
                      >
                        <option value="">Choisir une catégorie</option>
                        {categories.map((categorie) => (
                          <option
                            key={categorie._id}
                            name="category"
                            value={categorie._id}
                          >
                            {categorie.name}
                          </option>
                        ))}
                      </Field>
                      <ErrorMessage
                        name="category"
                        component="small"
                        className=""
                      />
                    <label className="">
                      Sous-catégories
                    </label>
                      <Field
                        as="select"
                        name="subcategory"
                        id="subcategory"
                        className=""
                      >
                        <option value="">Choisir une sous-catégorie</option>
                        {selected?.map((subcat) => (
                          <option
                            key={subcat._id}
                            name="subcategory"
                            value={subcat._id}
                          >
                            {subcat.name}
                          </option>
                        ))}
                      </Field>
                      <ErrorMessage
                        name="subcategory"
                        component="small"
                        className=""
                      />
                    <label className="">
                      Images
                    </label>
                      <input
                        multiple
                        type="file"
                        name="image"
                        className=""
                        onChange={(event) => {
                          setFieldValue("image", event.target.files[0]);
                        }}
                      />
                      <ErrorMessage
                        name="image"
                        component="small"
                        className=""
                      />
                  {values.image && <PreviewImage file={values.image} />}
                    <label className="">
                      Description
                    </label>
                      <Field
                        as="textarea"
                        id="description"
                        name="description"
                        className=""
                      />
                      <ErrorMessage
                        name="description"
                        component="small"
                        className=""
                      />
                    <label className="">
                      Statut en ligne
                    </label>
                      <Field
                        as="select"
                        id="online"
                        name="online"
                        className=""
                      >
                        <option value="true" name="online">
                          Actif
                        </option>
                        <option value="false" name="online">
                          Inactif
                        </option>
                      </Field>
                      
                      <ErrorMessage
                        name="online"
                        component="small"
                        className=""
                      />
                    <label className="">
                      Promotion
                    </label>
                      <Field
                        as="select"
                        id="promo"
                        name="promo"
                        className=""
                      >
                        <option value="" name="promo">
                          Ajouter une promotion
                        </option>
                        <option value="promodix" name="promo">
                          10% sur le prix
                        </option>
                        <option value="promoquinze" name="promo">
                          15% sur le prix
                        </option>
                        <option value="promovingt" name="promo">
                          20% sur le prix
                        </option>
                        <option value="promovingtcinq" name="promo">
                          25% sur le prix
                        </option>
                        <option value="promotrente" name="promo">
                          30% sur le prix
                        </option>
                      </Field>
                      <ErrorMessage
                        name="promo"
                        component="small"
                        className=""
                      />
                    <label className="">
                      Début promotion
                    </label>
                      <Field
                        type="date"
                        id="startpromo"
                        name="startpromo"
                        className=""
                      />
                      <ErrorMessage
                        name="startpromo"
                        component="small"
                        className=""
                      />
                    <label className="">
                      Fin promotion
                    </label>
                      <Field
                        type="date"
                        id="startpromo"
                        name="startpromo"
                        className=""
                      />
                      <ErrorMessage
                        name="startpromo"
                        component="small"
                        className=""
                      />
                  {/* <Field
                    name="subm"
                    type="submit"
                    disabled={isSubmitting}
                    defaultValue={isSubmitting ? "ENVOI EN COURS..." : "VALIDER LE PRODUIT"}
                  /> */}
                  <button
                    className="button_add_product"
                    type="submit"
                    disabled={isSubmitting}
                  >
                    {isSubmitting ? "ENVOI EN COURS..." : "VALIDER LE PRODUIT"}
                  </button>
                </Form>
              </div>
            )}
          </Formik>
        </div>
    </>
  );
};

export default AddProduct;
