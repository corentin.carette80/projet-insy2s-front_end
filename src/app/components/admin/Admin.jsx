import React from "react";
import useAdminHook from "./useAdminHook";
import Popup from "../admin/Popup";
import PreviewImage from "../admin/PreviewImage";
import { Formik, Form, Field, ErrorMessage } from "formik";
import NavAdmin from "../admin/NavAdmin";
import axios from "axios";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useNavigate } from "react-router-dom";
import Section from "../genericComponents/Section";
export default function Admin(props) {
  const {
    products,
    isOpen,
    formDisplay,
    arrayProduct,
    renderData,
    ReferenceError,
    currentItems,
    itemProduct,
    d,
    options,
    changeSelectOption,
    handleClick,
    pages,
    handleNextbtn,
    validate,
    categories,
    selected,
    setFormDisplay,
    setArrayProduct,
    setItemProduct,
    togglePopup,
    deleteProducts,
  } = useAdminHook(props);
  let navigate = useNavigate();
  let imag =0;
  return (
    <div className="min-h-screen flex flex-wrap bg-grey-custom">
      {/* Navigation */}

      <NavAdmin />

      {isOpen && setItemProduct && (
        <Popup
          content={
            <>
              <p>
                Êtes vous sûr d'effacer les données du produit ?
              </p>
                <button
                  className="button_add_product"
                  onClick={(e) => deleteProducts(itemProduct._id, e)}
                >
                  Supprimer le produit
                </button>
            </>
          }
          handleClose={togglePopup}
        />
      )}

      {/* Tableau des produits */}

      <div className="">
        {!formDisplay && !arrayProduct && renderData(currentItems)}

        {formDisplay && itemProduct && (
          <Formik
            initialValues={{
              id: itemProduct._id,
              name: itemProduct.name,
              price: itemProduct.price,
              licence: itemProduct.licence,
              caracteristics: itemProduct.feature,
              category: "",
              //category: itemProduct.categoryId,
              //subcategory: itemProduct.subCategoryId,
              subcategory: "",
              // image: "",
              
               image: itemProduct.image,
              stock: itemProduct.stock,
              description: itemProduct.description,
              etat: itemProduct.online,
            }}
            //validationSchema={validate}
            onSubmit={async (values) => {
              const { initialValues, ...data } = values;
              const formData = new FormData();
              formData.append("name", values.name);
              formData.append("price", values.price);
              formData.append("licence", values.licence);
              formData.append("stock", values.stock);
              formData.append("categoryId", [values.category]);
              formData.append("subCategoryId", [values.subcategory]);
              // formData.append("image", values.image);
               itemProduct.image && imag==0 ? "" : formData.append("image", values.image);
              formData.append("feature", values.caracteristics);
              formData.append("description", values.description);
              formData.append("online", true);


              const response = await axios
                .put(`http://localhost:5000/api/products/${data.id}`, formData)
                .then(() => {

                  toast.success("Produit modifié avec succès !", {
                    position: "top-center",
                    autoClose: 2500,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                  });
                  navigate("/admin");
                  // location.reload();
                })
                .catch((err) => {
                  if (err && err.response) console.log("Error: ", err);
                });
            }}
          >
            {({ values, isSubmitting, setFieldValue }) => (
              <span className="mr-auto">
                
                  {/* {d.toLocaleDateString("fr-FR", options)} */}
                
                <button
                  className="button_add_product"
                  onClick={() => {
                    setFormDisplay(!formDisplay);
                    setArrayProduct(arrayProduct);
                  }}
                >
                  &#x25C4; Retouner à la liste
                </button>
                <div className="addproduct_container">
                <Section section={"MODIFIER UN PRODUIT"} />
                <Form>
                    <label>
                      Nomination
                    </label>
                      <Field
                        type="text"
                        id="name"
                        name="name"
                      />
                      <ErrorMessage
                        name="name"
                        component="small"
                      />
                    <label>
                      Prix
                    </label>
                      <Field
                        type="number"
                        id="price"
                        name="price"
                      />
                      <ErrorMessage
                        name="price"
                        component="small"
                      />
                    <label>
                      Licence
                    </label>
                      <Field
                        type="text"
                        id="licence"
                        name="licence"
                      />
                      <ErrorMessage
                        name="licence"
                        component="small"
                      />
                    <label>
                      Caractéristique
                    </label>
                      <Field
                        type="text"
                        id="caracteristics"
                        name="caracteristics"
                      />
                      <ErrorMessage
                        name="caracteristics"
                        component="small"
                        
                      />
                    <label >
                      Quantité
                    </label>
                      <Field
                        type="number"
                        id="stock"
                        name="stock"
                        
                      />
                      <ErrorMessage
                        name="stock"
                        component="small"
                        
                      />
                    <label>
                      Catégories
                    </label>
                      <Field
                        as="select"
                        name="category"
                        id="category"
                        
                        onChange={(e) => {
                          changeSelectOption(e, setFieldValue);
                        }}
                      >
                        <option value="">Choisir une catégorie</option>
                        {categories.map((categorie) => (
                          <option
                            key={categorie._id}
                            name="category"
                            value={categorie._id}
                          >
                            {categorie.name}
                          </option>
                        ))}
                      </Field>
                      <ErrorMessage
                        name="category"
                        component="small"
                        
                      />
                    <label>
                      Sous-catégories
                    </label>
                      <Field
                        as="select"
                        name="subcategory"
                        id="subcategory"
                        
                      >
                        <option value="">Choisir une sous-catégorie</option>
                        {selected?.map((subcat) => (
                          <option
                            key={subcat._id}
                            name="subcategory"
                            value={subcat._id}
                          >
                            {subcat.name}
                          </option>
                        ))}
                      </Field>
                      <ErrorMessage
                        name="subcategory"
                        component="small"
                        
                      />
                    <label >
                      Images
                    </label>
                      <input
                        multiple
                        type="file"
                        name="image"
                        
                        onChange={(event) => {
                          setFieldValue("image", event.target.files[0]);
                          imag=1;
                        }}
                      />
                      <ErrorMessage
                        name="image"
                        component="small"
                        
                      />
                    {
                      itemProduct.image && imag==0 ? <img className="esp_ad" src={itemProduct.image} /> : values.image && <PreviewImage file={values.image} />
                    }

                    <label>
                      Description
                    </label>
                      <Field
                        as="textarea"
                        id="description"
                        name="description"
                        
                      />
                      <ErrorMessage
                        name="description"
                        component="small"
                        
                      />
                    <label >
                      Statut en ligne
                    </label>
                      <Field
                        as="select"
                        id="online"
                        name="online"
                        
                      >
                        <option value="true" name="online">
                          Actif
                        </option>
                        <option value="false" name="online">
                          Inactif
                        </option>
                      </Field>
                      <ErrorMessage
                        name="online"
                        component="small"
                       
                      />
                    <label >
                      Promotion
                    </label>
                      <Field
                        as="select"
                        id="promo"
                        name="promo"
                        
                      >
                        <option value="" name="promo">
                          Ajouter une promotion
                        </option>
                        <option value="promodix" name="promo">
                          10% sur le prix
                        </option>
                        <option value="promoquinze" name="promo">
                          15% sur le prix
                        </option>
                        <option value="promovingt" name="promo">
                          20% sur le prix
                        </option>
                        <option value="promovingtcinq" name="promo">
                          25% sur le prix
                        </option>
                        <option value="promotrente" name="promo">
                          30% sur le prix
                        </option>
                      </Field>
                      <ErrorMessage
                        name="promo"
                        component="small"
                       
                      />
                    <label >
                      Début promotion
                    </label>
                      <Field
                        type="date"
                        id="startpromo"
                        name="startpromo"
                        
                      />
                      <ErrorMessage
                        name="startpromo"
                        component="small"
                        
                      />
                    <label>
                      Fin promotion
                    </label>
                      <Field
                        type="date"
                        id="startpromo"
                        name="startpromo"
                        
                      />
                      <ErrorMessage
                        name="startpromo"
                        component="small"
                        
                      />
                  <button
                    className="button_add_product"
                    type="submit"
                    disabled={isSubmitting}
                  >
                    {isSubmitting ? "ENVOI EN COURS..." : "VALIDER LE PRODUIT"}
                  </button>
                </Form></div>
              </span>
            )}
          </Formik>
        )}
      </div>
    </div>
  );
}
