import React, { useEffect, useState } from 'react';
import NavAdmin from './NavAdmin';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import useAjouteruntheme from './useAjouteruntheme';
import Section from '../genericComponents/Section';

const AjouterUnTheme = () => {
    const {
        validateCategorie,
        validateSubCategorie,
        sleep,
        categories
      } = useAjouteruntheme();
    return (
        <>
            <NavAdmin />
            <div className="mx-auto w-2/6">
                <Formik
                    initialValues={{
                        name: '',
                    }}
                    validationSchema={validateCategorie}
                    onSubmit={async (values) => {
                        await sleep(2000);
                        const { initialValues, ...data } = values;
                        console.log(data);
                    }}
                >
                    {({ isSubmitting }) => (

                        <div className="addproduct_container">
                            <Section section={"AJOUTER UNE CATÉGORIE"} />
                            <Form className="esp_information">
                                    <label className=''>Catégorie</label>
                                        <Field type="text" name="name" className="" />
                                        <ErrorMessage name="name" component="small" className="" />
                                <button className="button_add_product" type="submit" disabled={isSubmitting}>
                                    {isSubmitting ? "ENVOI EN COURS..." : "VALIDER"}
                                </button>
                            </Form>
                        </div>
                    )}
                </Formik>
                <Formik
                    initialValues={{
                        upperCategory: '',
                        name: '',
                    }}
                    validationSchema={validateSubCategorie}
                    onSubmit={async (values) => {
                        await sleep(2000);
                        const { initialValues, ...data } = values;
                        console.log(data);
                    }}
                >
                    {({ isSubmitting })  => (
                        <div className="addproduct_container">
                        <Section section={"AJOUTER UNE SOUS-CATÉGORIE"} />
                            <Form className="esp_information">
                                    <label className="">Catégories</label>
                                        <Field as="select" name="name" className="">
                                            <option value="">Choisir une catégorie</option>
                                            {categories.map(
                                                categorie =>
                                                    <option key={categorie._id} name="name" value={categorie._id}>{categorie.name}</option>
                                            )}
                                        </Field>
                                        <ErrorMessage name="name" component="small" className="" />
                                    <label className=''>Sous-catégorie</label>
                                        <Field type="text" id="nameCat" name="nameCat" className="" />
                                        <ErrorMessage name="nameCat" component="small" className="" />
                                <button className="button_add_product" type="submit" disabled={isSubmitting}
                                >
                                    {isSubmitting ? "ENVOI EN COURS..." : "VALIDER"}
                                </button>
                            </Form>
                        </div>
                    )}
                </Formik>
            </div>
        </>
    );
};

export default AjouterUnTheme;