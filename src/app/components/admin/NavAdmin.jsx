import React from "react";
import { NavLink } from "react-router-dom";
import "../../assets/styles/admin.scss"
import {
  URL_ADMIN_ADD_PRODUIT,
  URL_ADMIN_ADD_THEME,
  URL_ADMIN_CHANGE_THEME,
  URL_ADMIN_HOME,
  URL_ADMIN_LOGOUT,
  URL_ADMIN_MANEGE_COMMENT,
  URL_ADMIN_MANEGE_ORDER,
  URL_ADMIN_MANEGE_RETURN,
  URL_ADMIN_MANEGE_USER,
  URL_ADMIN_MESSAGING,
} from "../../shared/constants/urls/urlConstants";
import Section from "../genericComponents/Section";

const NavAdmin = () => {
  return (
    <div className="espacement_admin">
      
        <NavLink exact to={URL_ADMIN_HOME}>
        <Section section={"TABLEAU D'ADMINISTRATION"} />
        </NavLink>
        <div className=" flex_nav_admin">
        <NavLink className="nav_admin_button" exact to={URL_ADMIN_ADD_PRODUIT}>
          AJOUTER UN PRODUIT
        </NavLink>
        <NavLink className="nav_admin_button" exact to={URL_ADMIN_ADD_THEME}>
          AJOUTER UNE CATÉGORIE
        </NavLink>
        <NavLink className="nav_admin_button" exact to={URL_ADMIN_CHANGE_THEME}>
          MODIFIER UNE CATÉGORIE
        </NavLink>
        <NavLink className="nav_admin_button" exact to={URL_ADMIN_MANEGE_ORDER}>
        COMMANDES / RETOURS
        </NavLink>
        <NavLink className="nav_admin_button" exact to={URL_ADMIN_MANEGE_USER}>
          GESTIONS D'UTILISATEUR
        </NavLink>
        <NavLink className="nav_admin_button" exact to={URL_ADMIN_MESSAGING}>
          MESSAGERIE
        </NavLink>
        <NavLink className="nav_admin_button" exact to={URL_ADMIN_LOGOUT}>
          QUITTER L'ADMINISTRATION
        </NavLink>
      </div>
    </div>
  );
};

export default NavAdmin;
