import React from "react";
import logo from "../../assets/images/logo.png";
import closeIcon from "../../assets/images/close_icon.png";
import HeaderLogo from "../header/HeaderLogo";

const Popup = props => {
    return (
        <div className="popup-box">
            <div className="box">
                <HeaderLogo/>
                <img src={closeIcon} className="close-icon" onClick={props.handleClose} alt="Fermer" />
                {props.content}
            </div>
        </div>
    );
};

export default Popup;