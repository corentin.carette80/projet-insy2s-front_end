import React, { useEffect, useState } from "react";
import axios from "axios";
import * as Yup from "yup";
import { NavLink } from "react-router-dom";
import { URL_ADMIN_ADD_PRODUIT } from "../../shared/constants/urls/urlConstants";
import { toast } from "react-toastify";
import DL from "../../assets/images/DL.png";
import Poubelle from "../../assets/images/Poubelle.png";
import Modifier from "../../assets/images/modifier.png";
import "react-toastify/dist/ReactToastify.css";
import Section from "../genericComponents/Section";
import { PDFDownloadLink } from "@react-pdf/renderer";
// import { Document, Page, Text } from "./reactPdf.js";

import pdf from "@react-pdf/renderer";
import DownloadLink from "../genericComponents/DownloadLink";
const { Document, Page, View, StyleSheet } = pdf;

const MyDoc = () => (
  <Document>
    <Page>
      <View>
        <div className="filminfo">
          <h1>Test</h1>
          <ul>{/* <li className="price_product">{val.price}€</li> */}</ul>
          <div className="desccription">
            <p className="titredescrpprod">Description</p>
            <p className="descriptionprod">{/* {val.description} */}</p>
          </div>
        </div>
      </View>
    </Page>
  </Document>
);

export default function useAdminHook(componentData) {
  const URLProducts = "http://localhost:5000/api/products/";
  const URLCategories = "http://localhost:5000/api/category/";
  const URLSubCategories = "http://localhost:5000/api/subCategory/";

  const [products, setProducts] = useState([]);
  const [categories, setCategories] = useState([]);
  const [subCategories, setSubCategories] = useState([]);
  const [formDisplay, setFormDisplay] = useState(false);
  const [arrayProduct, setArrayProduct] = useState(false);
  const [searchTerm, setSearchTerm] = useState("");
  const [selected, setSelected] = useState();
  const [isOpen, setIsOpen] = useState(false);
  const [itemProduct, setItemProduct] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [itemsPerPage, setItemsPerpage] = useState(10);
  const [pageNumberLimit, setPageNumberLimit] = useState(10);
  const [maxPageNumberLimit, setMaxPageNumberLimit] = useState(5);
  const [minPageNumberLimit, setMinPageNumberLimit] = useState(0);

  // Data des produits

  useEffect(() => {
    axios.get(URLProducts).then((response) => {
      setProducts(response.data.products);
    });
  }, []);

  // Data des catégories

  useEffect(() => {
    axios.get(URLCategories).then((response) => {
      setCategories(response.data.category);
    });
  }, []);

  // Data des sous-catégories

  useEffect(() => {
    axios.get(URLSubCategories).then((response) => {
      setSubCategories(response.data.subCategory);
    });
  }, []);

  // Value de l'input de recherche

  const handleChange = (event) => {
    setSearchTerm(event.target.value);
  };

  // Ouverture / fermeture Popup du delete product

  const togglePopup = () => {
    setIsOpen(!isOpen);
  };

  const ficheprod = (id) => {
    return axios
      .get(`http://localhost:5000/api/pdf/product/${id}`, {
        responseType: "arraybuffer",
        headers: {
          Accept: "application/pdf",
        },
      })
      .then((response) => {
        const blob = new Blob([response.data], { type: "application/pdf" });
        const link = document.createElement("a");
        link.href = window.URL.createObjectURL(blob);
        link.download = `product_${id}.pdf`;
        link.click();
        this.closeModal(); // close modal
      })
      .catch((err) => console.log(err));
  };
  // Function axios delete product

  const deleteProducts = async (_id, e) => {
    e.preventDefault();
    var index = products.indexOf(_id);
    products.splice(index, 1);
    const response = await axios
      .delete(`${URLProducts}${_id}`)
      .then(() => {
        setProducts(products);
        toast("❌ Produit supprimé avec succès !", {
          position: "top-center",
          autoClose: 2500,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      })
      .catch((err) => console.log(err));
    setIsOpen(!isOpen);
  };

  useEffect(() => {}, [products]);

  // Si pas de produit, retourne null

  if (!products) return null;

  // Gestion de la date

  const d = new Date();
  const options = {
    weekday: "long",
    day: "numeric",
    month: "numeric",
    year: "numeric",
  };

  // Select de la subcategory par rapport à la catégory

  const changeSelectOption = (e, setFieldValue) => {
    const selectedId = e.target.value;
    const selectedSubcategory = categories.find((d) => d._id === selectedId);
    setSelected(selectedSubcategory.subCategories);
    setFieldValue("category", selectedId);
  };

  // ID des pages (pagnination)

  const handleClick = (e) => {
    setCurrentPage(Number(e.target.id));
  };

  // Calcule des pages

  const pages = [];
  for (let i = 1; i <= Math.ceil(products.length / itemsPerPage); i++) {
    pages.push(i);
  }
  // ki
  // Gestion du btn Next

  const handleNextbtn = () => {
    setCurrentPage(currentPage + 1);

    if (currentPage + 1 > maxPageNumberLimit) {
      setMaxPageNumberLimit(maxPageNumberLimit + pageNumberLimit);
      setMinPageNumberLimit(minPageNumberLimit + pageNumberLimit);
    }
  };

  // Gestion du btn Prev

  const handlePrevbtn = () => {
    setCurrentPage(currentPage - 1);

    if ((currentPage - 1) % pageNumberLimit == 0) {
      setMaxPageNumberLimit(maxPageNumberLimit - pageNumberLimit);
      setMinPageNumberLimit(minPageNumberLimit - pageNumberLimit);
    }
  };

  // Btn incrementation "..."

  let pageIncrementBtn = null;
  if (pages.length > maxPageNumberLimit) {
    pageIncrementBtn = (
      <li
        className="p-2 border border-solid border-black bg-white cursor-pointer hover:bg-black hover:text-white"
        onClick={handleNextbtn}
      >
        {" "}
        &hellip;{" "}
      </li>
    );
  }

  // Btn decrementation "..."

  let pageDecrementBtn = null;
  if (minPageNumberLimit >= 1) {
    pageDecrementBtn = (
      <li
        className="p-2 border border-solid border-black bg-white cursor-pointer hover:bg-black hover:text-white"
        onClick={handlePrevbtn}
      >
        {" "}
        &hellip;{" "}
      </li>
    );
  }

  // Btn incrémentation produits

  const handleLoadMore = () => {
    setItemsPerpage(itemsPerPage + 5);
  };

  // Calcule du dernier, premier et tous les produits

  const indexOfLastitem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastitem - itemsPerPage;
  const currentItems = products.slice(indexOfFirstItem, indexOfLastitem);

  // Rendu des nombres de la pagnination

  const renderPageNumbers = pages.map((number) => {
    if (number < maxPageNumberLimit + 1 && number > minPageNumberLimit) {
      return (
        <li className="test" key={number} id={number} onClick={handleClick}>
          {number}
        </li>
      );
    } else {
      return null;
    }
  });

  // Rendu du tableau des produits + pagnination

  const renderData = (products) => {
    // let MyDoc;
    return (
      <>
        <Section section={"TOUS LES PRODUITS"} />
        <div className="flex justify-center mb-10">
          <input
            className="recherche_admin"
            type="text"
            placeholder="Rechercher un produit"
            value={searchTerm}
            onChange={handleChange}
          />
        </div>
        <div className="table_admin">
          <table className="">
            <thead>
              <tr className="">
                <th className="">
                  <input type="checkbox" />
                </th>
                <th className="">DENOMINATION</th>
                <th className="">PRIX</th>
                <th className="">CATÉGORIE</th>
                <th className="">DATE</th>
                <th className="">STOCK</th>
                <th className="">ACTIONS</th>
              </tr>
            </thead>
            <tbody>
              {products
                .filter((val) => {
                  return val.name
                    .toLowerCase()
                    .includes(searchTerm.toLowerCase());
                })
                .map((val) => {
                  // const MyDoc = () => (
                  //   <Document>
                  //     <Page>
                  //       <View>
                  //         <div className="filminfo">
                  //           <h1>{val.name}</h1>
                  //           <ul>
                  //             <li className="price_product">{val.price}€</li>
                  //           </ul>
                  //           <div className="desccription">
                  //             <p className="titredescrpprod">Description</p>
                  //             <p className="descriptionprod">
                  //               {val.description}
                  //             </p>
                  //           </div>
                  //         </div>
                  //       </View>
                  //     </Page>
                  //   </Document>
                  // );

                  return (
                    <tr key={val._id}>
                      <th className="">
                        <input type="checkbox" />
                      </th>
                      <td className="">{val.name}</td>
                      <td className="">{val.price} €</td>
                      <td className="">{val.categoryId[0].name}</td>
                      <td className="">
                        {console.log(val.createdAt)}
                        {new Date(val.createdAt).toLocaleDateString("fr-FR")}
                      </td>
                      <td className="">{val.stock}</td>
                      <td>
                        <ul className="actions_admin">
                          <li className="">
                            <img
                              src={Modifier}
                              alt=""
                              onClick={() => {
                                setFormDisplay(!formDisplay);
                                setArrayProduct(arrayProduct);
                                setItemProduct(val);
                              }}
                            />
                          </li>
                          <li className="">
                            <img
                              src={Poubelle}
                              alt=""
                              onClick={() => {
                                togglePopup();
                                setItemProduct(val);
                              }}
                            />
                          </li>
                          <li className="">
                            <img
                              src={DL}
                              onClick={() => {
                                ficheprod(val._id);
                              }}
                            />
                          </li>
                        </ul>
                      </td>
                    </tr>
                  );
                })}
            </tbody>
          </table>
        </div>
        <div className="pagination">
          <div className="previous">
            <button
              className="left"
              onClick={handlePrevbtn}
              disabled={currentPage == pages[0] ? true : false}
            >
              Précédent
            </button>
          </div>
          {/* <div className="flex"> */}
          <ul className="pages">{renderPageNumbers}</ul>
          {/* </div> */}
          <div className="next">
            <button
              className="right"
              onClick={handleNextbtn}
              disabled={currentPage == pages[pages.length - 1] ? true : false}
            >
              Suivant
            </button>
          </div>
        </div>

        <NavLink
          className="button_add_product"
          exact
          to={URL_ADMIN_ADD_PRODUIT}
        >
          Ajouter un produit
        </NavLink>
      </>
    );
  };

  // Vérification format d'image

  const SUPPORTED_FORMATS = [
    "image/jpg",
    "image/jpeg",
    "image/gif",
    "image/png",
  ];

  // Validation Yup ajout de produit

  const validate = Yup.object({
    name: Yup.string()
      .max(80, "Ne pas dépasser plus de 80 caractères")
      .required("Veuillez mettre un nom de produit"),
    price: Yup.number()
      .min(1, "Veuillez mettre au moins un chiffre")
      .positive("Veuillez mettre un chiffre positif")
      .required("Veuillez mettre un prix"),
    licence: Yup.string()
      .max(25, "Ne pas dépasser plus de 25 caractères")
      .required("Veuillez mettre une licence"),
    stock: Yup.number()
      .min(0, "Veuillez mettre un chiffre")
      .required("Veuillez mettre un nombre de stock"),
    category: Yup.string().required("Veuillez choisir une catégorie"),
    subcategory: Yup.string().required("Veuillez choisir une sous-catégorie"),
    description: Yup.string()
      .max(250, "Ne pas dépasser plus de 250 caractères")
      .required("Veuillez mettre une description"),
    caracteristics: Yup.string()
      .max(150, "Ne pas dépasser plus de 150 caractères")
      .required("Veuillez mettre une caractéristique"),
  });
  return {
    products,
    renderData,
    isOpen,
    formDisplay,
    arrayProduct,
    ReferenceError,
    currentItems,
    itemProduct,
    d,
    options,
    changeSelectOption,
    handleClick,
    pages,
    handleNextbtn,
    validate,
    categories,
    selected,
    setFormDisplay,
    setArrayProduct,
    setItemProduct,
    togglePopup,
    deleteProducts,
  };
}
