import React, { useState } from "react";
import { Formik, Form } from "formik";
import { TextField } from "../../components/account/TextField";
import { Link } from "react-router-dom";
import {
  URL_RESET_PASSWORD,
  URL_SIGN_UP,
} from "../../shared/constants/urls/urlConstants";
import Modal from "../ResetPassword/Modal";
import "../../assets/styles/connexion.scss"
import "react-toastify/dist/ReactToastify.css";
import Connecter from "./Connecter"
import Section from "../genericComponents/Section";

const ConnectView = (props) => {
    const { onSubmit, validate, message,email ,password, loading } = Connecter(props);
    return (
            <div className="connect_container">
            <Section section={"Se connecter"} />
      {message && (
        <div className="form-group">
          <div className="alert alert-danger" role="alert">
            {message}
          </div>
        </div>
      )}
      <Formik
        initialValues={{
          email: "",
          password: "",
        }}
        onSubmit={onSubmit}
        validationSchema={validate}
      >
        {(formik) => (
          
            <Form>
              <TextField
                label="Email"
                name="email"
                type="email"
              />
              <TextField
                label="Mot de passe"
                name="password"
                type="password"
              />
              <p >
              Mot de passe oublié  ?
              <button 
              type="button" 
              onClick={()=> setShowModal(true)}>
              </button>
              <Modal/>
              </p>

              <div >
                <button className="button_contact centre_contact">Se connecter</button>

                <p>
                  Je n'ai pas de compte ?
                  <Link to={URL_SIGN_UP}>
                    Je m'inscris
                  </Link>
                </p>
              </div>
            </Form>
          
        )}
      </Formik>
      </div>
    );
};

export default ConnectView;