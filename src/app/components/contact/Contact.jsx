import React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import useContactHook from "./useContactHook";
import emailjs from "emailjs-com";
import { string } from "yup";
import { toast } from "react-toastify";
import Section from "../genericComponents/Section";


function Contact(props) {
  const { initialValues, userSchema } = useContactHook(props);
 

  const SendEmail = (object) => {
    emailjs
      .send("service_r46xuko", "template_r3x2ln6", object, "eNGDOYRnJO0pijwMX")
      .then(
        /*modif ici */
        () => {
          toast.success("👏 message envoyé avec succès ", {
            position: "top-center",
            autoClose: 2500,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
          navigate("/home");
          location.reload();
          /*fin modif*/
        },
        (result) => {
          console.log(result.text);
        },
        
        (error) => {
          console.log(error.text);
        }
      );
  };
  
  let user=JSON.parse(localStorage.getItem("user"))
  console.log(JSON.parse(localStorage.getItem("user")))
  
  return (
    <div className="contact_container">
      <Section section={"Contact"} />
      <Formik
        initialValues={initialValues}
        onSubmit={(values) => {
          SendEmail(values);
        }}
        validationSchema={userSchema}
      >
          <Form>
              <p>
                Nous sommes à votre écoute pour toutes questions. Veuillez nous
                laisser <br /> votre message et nous vous recontacterons au plus
                vite
              </p>
              <label  htmlFor="firstName">
            Prénom
          </label>
              <Field
                name="firstName"
                type="text"
                placeholder="Veuillez entrer votre nom svp"
              />
              <span>
                <ErrorMessage name="firstName" />
              </span>
              <label  htmlFor="lastName">
            Nom
          </label>
              <Field
                name="lastName"
                type="text"
                placeholder="Veuillez entrer votre prénom svp"
              />
              <span>
                <ErrorMessage name="lastName" />
              </span>
              <label  htmlFor="mail">
            Adresse-mail
          </label>
              <Field
                name="mail"
                type="text"
                placeholder="Veuillez entrer votre adresse mail "  
              />
              <span >
                <ErrorMessage name="mail" />
              </span>
              <label  htmlFor="mobil">
            Tél portable
          </label>
              <Field
                name="mobil"
                type="number"
                placeholder="Veuillez entrer votre numéro de téléphone (optionnel)"
              />
              <span >
                <ErrorMessage name="mobil" />
              </span>
              <label  htmlFor="commande">
            Tél fixe
          </label>
              <Field
                name="commande"
                type="number"
                placeholder="Veuillez entrer votre numéro de commande (optionnel)"
              />
              <label  htmlFor="message">
            Message
          </label>
              <Field
                name="message"
                className="message"
                type="text"
                placeholder="Veuillez saisir votre texte ici"
              ></Field>
              <span >
                <ErrorMessage name="message" />
              </span>
              <Field
            name="subm"
            type="submit"
            defaultValue="Enregistrer"
          />
          </Form>
      </Formik>
    </div>
  );
}

export default Contact;
