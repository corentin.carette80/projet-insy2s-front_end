import React from 'react';
import { Link } from 'react-router-dom';
import '../../assets/styles/favoris.scss';
import Rien from "../../assets/images/rien.jpg";
import Section from '../genericComponents/Section';
import Favoris1 from "../../assets/images/favoris2.png";
const Favoris = () => {
    let favoris=JSON.parse(localStorage.getItem("Favorites"))
    console.log(favoris)

    const [products, setProducts] = React.useState(favoris);

      const onRemoveProduct = (i) => {
        const filteredProduct = products.filter((product, index) => {
          return index != i;
        });
        localStorage.setItem("Favorites", JSON.stringify(filteredProduct));
        setProducts(filteredProduct);
      };
    return (
        <div className='containerfav'>
            <Section section={"Mes Favoris"} />
            <div className='itemfav'>
            {favoris.map((product, index) => {
            console.log(product)
            return(
              
            <div key={index}>
                <div className="promo-item" key={product._id}>
                <div className='filmtitle'
                            onClick={() => onRemoveProduct(index)}
                        >
                            <img className="favoris-icon" src={Favoris1} />
                        </div>
                {product.image ? (
                  <Link className="product-link" to={`/product/${product._id}`}>
                    <img src={product.image} className="promo-item-img" />
                  </Link>
                ) : (
                  <Link className="product-link" to={`/product/${product._id}`}>
                    <img src={Rien} className="promo-item-img" />
                  </Link>
                )}
                <div className="product-infos">
                  <Link className="centre" to={`/product/${product._id}`}>
                    <p className="centre">{product.name}</p>
                  </Link>
                        <Link to={`/product/${product._id}`}>
                          <p className="affichage_prix">{product.price} €</p>
                        </Link>
                </div>
              </div>
            </div>
            )
            
            })}
        </div>
        </div>
    );
};

export default Favoris;