import React from "react";
import Logo from "../../assets/images/logo.png";
import {
  URL_FOOT_CONTACT,
  URL_LOGIN,
  URL_PROFILE,
  URL_RAYONS,
} from "../../shared/constants/urls/urlConstants";
import { Link } from "react-router-dom";
import FooterLogo from "../footer/FooterLogo";

const Footer = () => {
  return (
    <div className="footer-section">
      <div className="footer">
        <div className="footer-header">
          <FooterLogo />
          <div class="footer-social-icon">
            {/* <span>Follow us</span> */}
            <a href="#">
              <i class="fab fa-facebook-f facebook-bg"></i>
            </a>
            <a href="#">
              <i class="fab fa-twitter twitter-bg"></i>
            </a>
            <a href="#">
              <i class="fab fa-google-plus-g google-bg"></i>
            </a>
          </div>
        </div>
        <div className="menu-footer links">
          <h3 className="quick-links-title">Liens rapides</h3>
          <hr className="quick-links" />
          <div className="menu_footer">
            <ul>
              <li>
                <Link to={URL_PROFILE}> Mon compte </Link>
              </li>
              <li>
                <Link to={URL_FOOT_CONTACT}> Nous contacter </Link>
              </li>
              <li>Retours</li>
            </ul>

            <ul>
              <li>A propos</li>
              <li>
                <Link to={URL_RAYONS}> Boutique </Link>
              </li>
              <li>FAQ</li>
            </ul>

            <ul className="margin-top-ul">
              <li>Mentions Légales</li>
              <li>Utilisation des cookies</li>
              <li>Conditions générales de ventes</li>
            </ul>
          </div>
        </div>
      </div>
      <div className="copyright">
        {" "}
        <p>Copyright © 2022 Cinélounge.</p>{" "}
      </div>
    </div>
  );
};

export default Footer;
