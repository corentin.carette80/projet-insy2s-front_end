import React from "react";
import { Link } from "react-router-dom";
const Button = (props) => {
  return (
    <Link to={props.url} className="button">
      {props.link}
    </Link>
  );
};

export default Button;
