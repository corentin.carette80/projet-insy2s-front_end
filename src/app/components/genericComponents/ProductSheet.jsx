import React from "react";
import { Link } from "react-router-dom";
import Favorites from "../Favorites/Favorites";

const ProductSheet = (props) => {
  return (
    <div className="promo-item">
      <Favorites product={props.product} id={props.id} />
      <Link className="product-link" to={`/product/${props.id}`}>
        <img className="promo-item-img" src={props.image} />
      </Link>
      <div className="product-infos">
        <p className="centre">{props.title}</p>
        <p className="affichage_prix">{props.price} €</p>
      </div>
    </div>
  );
};

export default ProductSheet;
