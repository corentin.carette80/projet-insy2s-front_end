import React from "react";

const Section = (props) => {
  return (
    <>
      <h3 className="promo-title">{props.section}</h3>
      <hr className="section-line" />
    </>
  );
};

export default Section;
