import React from "react";
import Logo from "../../assets/images/header-logo2.png";

const HeaderLogo = () => {
  return (
    <div className="header-logo header-logo-top">
      <div class="container">
        <a href="/">
          <img src={Logo} alt="" />
        </a>
        {/* <span class="line-2">CINELOUNGE</span> */}
        {/* <span class="line-1">the</span> */}
        <span class="line-3">articles de cinéma</span>
        <span class="line-5">
          <a href="/">goodies</a>
          <a href="/">films / séries</a>
        </span>
        {/* <span class="line-6">2022</span> */}
      </div>
      {/* <a href="/">
        <img className="logo" src={Logo} alt="" />
      </a> */}
    </div>
  );
};

export default HeaderLogo;
