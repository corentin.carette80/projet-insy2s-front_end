import React from "react";
import { Link } from "react-router-dom";
import Panier from "../../assets/images/Panier.png";
import {
  URL_PROFILE,
  URL_PANIER,
} from "../../shared/constants/urls/urlConstants";
const Navigation = () => {
  return (
    <div className="navigation-wrapper">
      <div className="containerSearch">
        <div className="cselect">
          <select className="filters" name="filters" id="filters-input">
            <option selected disabled value="">
              Catégories
            </option>
            <option value="">Films </option>
            <option value="">Séries </option>
            <option value="">Goodies </option>
          </select>
        </div>
        <div className="truc">
        <input
          placeholder="Rechercher un produit..."
          className="searchBar js-search"
          type="text"
        />
        <i className="fa fa-search"></i></div>
      </div>
      <div className="icon_right">
        <a className='side_profile' href={URL_PANIER}>
                  <img className="pan_nav" src={Panier}></img>
      </a>
      <a className=" side_profile" href="/profile"><i className="fas fa-user"></i></a>
      </div>
      
    </div>
  );
};

export default Navigation;
