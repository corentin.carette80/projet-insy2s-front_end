import React from "react";
import { Link } from "react-router-dom";
import Logo from "../../assets/images/logo.png";
import Panier from "../../assets/images/Panier.png";
import Dark from "../../assets/images/Mode dark.png";
import User from "../../assets/images/user.png";
import { ToggleButton } from "../toggleButton/toggleButton";
import "../../assets/styles/ShoppingCart.scss";
import {
  URL_NAV_CARD,
  URL_NAV_NEW,
  URL_NAV_TOP,
  URL_NAV_SHOP,
  URL_LOGIN,
  URL_PROFILE,
  URL_HOME,
  URL_SEARCH_PAGE,
  URL_SEARCH,
  URL_PANIER,
} from "../../shared/constants/urls/urlConstants";
import usenavhash from "./usenavhash";
import { push as Menu } from "react-burger-menu";
// import "../../assets/styles/searchBar.scss";

export default function Navbar() {
  const {
    onTextChange,
    myFunction,
    selectCategory,
    toggleNavSmallScreen,
    toggleModal,
    categorySearchNav,
    toggleDarkTheme,
    searchText,
    isOpen,
    isToggle,
  } = usenavhash();
  const showSettings = (event) => {
    event.preventDefault();
  };

  return (
    // <div className="wrapper-menu">
    <div className="wrapper-nav">
      <Menu pageWrapId={"page-wrap"} outerContainerId={"outer-container"}>
        <a id="home" className="menu-item" href="/">
          Accueil
        </a>
        <a id="about" className="menu-item" href="/rayons">
          Rayons
        </a>
        <a id="contact" className="menu-item" href="/topsales">
          Top Ventes
        </a>
        <a id="contact" className="menu-item" href="/news">
          Nouveauté
        </a>
        <a id="contact" className="menu-item" href="/cadeau">
          Carte Cadeau
        </a>
        {/* <a onClick={showSettings} className="menu-item--small" href="">
          Nouveautés
        </a> */}
        {/* <div className="containerSearchNav">
          <div className="cselectNav">
            <select className="filtersNav" name="filters" id="filters-input">
              <option selected disabled value="">
                Catégories
              </option>
              <option value="">Films </option>
              <option value="">Séries </option>
              <option value="">Goodies </option>
            </select>
          </div>
          <input
            placeholder="Rechercher un produit..."
            className="searchBarNav js-search"
            id="searchBarNav"
            type="text"
          />
          <i id="searchBarIcon" className="fa fa-search"></i>
        </div> */}
        
          <a className="btn-sidebar side_profile" href="/profile"><i className="fas fa-user"></i></a>
        
      </Menu>
    </div>
    // </div>
  );
}
