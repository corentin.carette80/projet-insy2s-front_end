import React from "react";
import PaginationHook from "./usePaginationHook";
const Pagination = (props) => {
  let data = props;
  const {
    currentPage,
    pageNumberLimit,
    maxPageNumberLimit,
    minPageNumberLimit,
    itemsPerPage,
    indexOfFirstItem,
    indexOfLastitem,
    onTrigger,
    handleClick,
    handleNextbtn,
    handlePrevbtn,
    renderPageNumbers,
  } = PaginationHook(data);

  return (
    <>
      {/* if(props.empty) ? rendre rien : rendre pagination */}
      {props.nofilter ? (
        <span></span>
      ) : renderPageNumbers < 1 ? null : (
        <div className="pagination">
          <div className="previous">
            <button
              className="left"
              onClick={handlePrevbtn}
              disabled={currentPage == props.pages[0] ? true : false}
            >
              Précédent
            </button>
          </div>
          {/* <div className="flex"> */}
          <ul className="pages">{renderPageNumbers}</ul>
          {/* </div> */}
          <div className="next">
            <button
              className="right"
              onClick={handleNextbtn}
              disabled={
                currentPage == props.pages[props.pages.length - 1]
                  ? true
                  : false
              }
            >
              Suivant
            </button>
          </div>
        </div>
      )}
    </>
  );
};

export default Pagination;
