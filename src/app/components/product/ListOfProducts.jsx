import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Rien from "../../assets/images/rien.jpg";
import Favorites from "../Favorites/Favorites";
import Buy from "../Buy/Buy";
import EmptySearch from "../../views/EmptySearch";
import ProductSheet from "../genericComponents/ProductSheet";

const ListOfProducts = (props) => {
  return (
    <>
      {/* <div className="topventeglob"> */}
      <div className="listOfProducts">
        {props.toLoop.length === 0 ? (
          <EmptySearch />
        ) : (
          props.toLoop.map((product) => (
            <ProductSheet
              product={product}
              title={product.name}
              image={product.image ? product.image : Rien}
              price={product.price}
              id={product._id}
            />
          ))
        )}
      </div>
      {/* </div> */}
    </>
  );
};

export default ListOfProducts;
