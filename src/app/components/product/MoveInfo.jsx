import React from "react";
import "../../assets/styles/index.scss";
import Rien from "../../assets/images/rien.jpg";
import Buy2 from "../Buy/Buy2";
import Favorites from "../Favorites/Favorites";
import Section from "../genericComponents/Section";
export default function MoveInfo(props) {
  const options = { year: "numeric" };
  const button = true;
  return (
    <>
      <div className="moveinfo_container">
        <div className="separation_product">
          <Favorites product={props.product} />
          {props.product.image ? (
            <img src={props.product.image} className="picture_product" />
          ) : (
            <img src={Rien} className="picture_product" />
          )}
        </div>
        <div className="filminfo">
            <h1 >{props.product.name}</h1>
            <ul>
              <li className="dateprod">
                {new Date(props.product.updatedAt).toLocaleDateString(
                  "fr-FR",
                  options
                )}
              </li>
              <li>
                {props.product.subCategoryId?.map((subcat) => (
                  <option
                    className=""
                    key={subcat._id}
                    name="subcategory"
                    value={subcat._id}
                  >
                    {subcat.name}
                  </option>
                ))}
              </li>
              <li className="price_product">{props.product.price}€</li>
            </ul>
          <div className="desccription">
                <p className="titredescrpprod">Description</p>
              <p className="descriptionprod">
                {props.product.description}
              </p>
          </div>
          <div>
            {props.product.stock > 0 ? (
              <div>
                {props.product.stock > 0 ? <Buy2 product={props.product} /> : <p></p>}
              </div>
            ) : (
              <div className="divunstock">
                <p className="stock">plus de stock</p>
              </div>
            )}
          </div>
          
        </div>
      </div>
    </>
  );
}
