import React, { useState } from "react";
import { useLocation } from "react-router-dom";

const useListOfProducts = (componentData) => {
  let toLoop;
  componentData.searchTerm
    ? (toLoop = componentData.products.filter((val) => {
        if (componentData.searchTerm == "") {
          // setEmpty(false);
          return val;
        } else if (
          val.name
            .toLowerCase()
            .includes(componentData.searchTerm.toLowerCase())
        ) {
          // setEmpty(false);
          return val;
        }
        // else if (componentData.searchTerm !== val.name.toLowerCase()) {
        //   console.log("boucle");
        //   setEmpty(!empty);
        //   return val;
        // }
      }))
    : (toLoop = componentData.products);

  console.log(toLoop);

  const location = useLocation();
  const options = { year: "numeric" };
  return { toLoop };
};

export default useListOfProducts;
