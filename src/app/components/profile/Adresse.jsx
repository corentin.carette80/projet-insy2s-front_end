import React from "react";
import Section from "../genericComponents/Section";
import { Field, Form, Formik } from "formik";
import axios from "axios";
const Adresse = () => {
  let user = JSON.parse(localStorage.getItem("user"));
  const initialValueshouse = {
    namehouse: user.address ? user.address.namehouse : "",
    streetNumber: user.address ? user.address.streetNumber : "",
    street: user.address ? user.address.street : "",
    floor: user.address ? user.address.floor : "",
    city: user.address ? user.address.city : "",
    country: user.address ? user.address.country : "",
  };
  console.log(user);
  return (
    <div className="mes_information_container">
      <Section section={"Mes Adresses"} />
      <Formik
        initialValues={initialValueshouse}
        onSubmit={(values) => {
          axios.put(
            `http://localhost:5000/api/test/address/${user.addressId}`,
            values
          );
          // axios.put(`http://localhost:5000/api/test/${user.id}`, values);
          console.log(user.id);
          console.log(values);
        }}
      >
        <Form className="esp_information">
          <label htmlFor="namehouse">
            <p>Nom</p>
          </label>
          <Field
            name="namehouse"
            type="text"
            placeholder="Nom"
            id="namehouse"
            values={user?.address?.namehouse}
          />
          <label htmlFor="streetNumber">Numéro de la rue</label>
          <Field
            name="streetNumber"
            type="number"
            placeholder="Numéro de la rue"
            id="streetNumber"
            values={user?.address?.streetNumber}
          />

          <label htmlFor="street">Rue</label>
          <Field name="street" type="text" placeholder="Rue" id="street" />

          <label htmlFor="floor">Numéro d'appartement</label>
          <Field
            name="floor"
            type="number"
            placeholder="Numéro d'appartement"
            id="floor"
            values={user?.address?.floor}
          />

          <label htmlFor="city">Ville</label>
          <Field
            name="city"
            type="text"
            placeholder="Ville"
            id="city"
            values={user?.address?.city}
          />
          <label htmlFor="country">Pays</label>
          <Field
            name="country"
            type="text"
            placeholder="Pays"
            id="country"
            values={user?.address?.country}
          />
          <Field name="subm" type="submit" defaultValue="Enregistrer" />
        </Form>
      </Formik>
    </div>
  );
};

export default Adresse;
