import { Field, Form, Formik } from "formik";
import React, { useState } from "react";
import "../../assets/styles/profile.scss";
import * as yup from "yup";
import Section from "../genericComponents/Section";
import axios from "axios";
const Information = () => {
  const [convertisseur, setConvertisseur] = useState(false);

  const changeInput = (e) => {
    setConvertisseur(!convertisseur);
  };

  let user = JSON.parse(localStorage.getItem("user"));
  const initialValues = {
    firstName: user ? user.firstName : "",
    lastName: user ? user.lastName : "",
    email: user ? user.email : "",
    portable: "",
    fixe: "",
    MDP: "",
  };
  console.log("coucour c'est moi")
  return (
    <div className="mes_information_container">
      <Section section={"Mes Informations"} />
      <Formik
        initialValues={initialValues}
        onSubmit={(values) => {
          axios.put(`http://localhost:5000/api/test/${user.id}`, values);
        }}
      >
        <Form className="esp_information">
          <label  htmlFor="nom">
            <p>Nom</p>
          </label>
          <Field
            name="lastName"
            type="text"
            placeholder="nom"
            id="nom"
          />
          <label htmlFor="prenom">
            Prénom
          </label>
          <Field
            name="firstName"
            type="text"
            placeholder="prenom"
            id="prenom"
          />

          <label htmlFor="mail">
            Adresse-mail
          </label>
          <Field
            name="email"
            type="text"
            placeholder="mail"
            id="mail"
          />

          <label htmlFor="portable">
            Tél portable
          </label>
          <Field
            name="portable"
            type="number"
            placeholder="téléphone portable"
            id="portable"
          />

          <label htmlFor="fixe">
            Tél fixe
          </label>
          <Field
            name="fixe"
            type="number"
            placeholder="téléphone fixe"
            id="fixe"
          />

          <label htmlFor="MDP">
            Mot de passe
          </label>
          <Field
            name="MDP"
            type={convertisseur ? "text" : "password"}
            placeholder="Mot de passe"
            id="MDP"
          />
          <label className="invi_inf"> 
            
          <input
            type="checkbox"
            defaultValue="text"
            onClick={changeInput}
          /><span><i className="fa fa-search see_inf"></i></span>
          </label>
          

          <Field
            name="subm"
            type="submit"
            defaultValue="Enregistrer"
          />
        </Form>
      </Formik>
    </div>
  );
};

export default Information;
