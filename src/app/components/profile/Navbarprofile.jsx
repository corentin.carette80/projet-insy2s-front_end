import React from "react";
import { NavLink } from "react-router-dom";
import {
  URL_PROFILE,
  URL_ADDRESSES,
  URL_ENVIE,
  URL_RETOURS,
  URL_COMMANDES,
  URL_PAIEMENT,
  URL_LOGIN,
  URL_NAV_CARD,
  URL_UPDATE_PASSWORD,
} from "../../shared/constants/urls/urlConstants";
import Section from "../genericComponents/Section";
const Navbarprofile = () => {
  const deco = () => {
    localStorage.removeItem("user");
  };
  return (
    <div className="espacement_top_profil">
      <Section section={"Mon profile"} />
      <div className="flex_nav_profile">
        <NavLink className="nav_admin_button" exact to={URL_PROFILE}>
          Mes Informations
        </NavLink>
        <NavLink className="nav_admin_button" exact to={URL_UPDATE_PASSWORD}>
          Mon Mot De Passe
        </NavLink>
        <NavLink className="nav_admin_button" exact to={URL_ENVIE}>
          Favoris
        </NavLink>
        <NavLink className="nav_admin_button" exact to={URL_PAIEMENT}>
          Vos Paiement
        </NavLink>
        <NavLink className="nav_admin_button" exact to={URL_COMMANDES}>
          <span>Retours</span> &amp; Commandes
        </NavLink>
        <a href={URL_LOGIN} onClick={deco} className="nav_admin_button centredeco">
          Se Déconnecter
        </a>
      </div>
    </div>
  );
};

export default Navbarprofile;
