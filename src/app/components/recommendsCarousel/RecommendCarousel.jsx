import React from "react";
import RecommendCarouselHook from "./useRecommendCarouselHook";

const RecommendCarousel = (props) => {
  let data = props;
  const {
    currentPage,
    pageNumberLimit,
    maxPageNumberLimit,
    minPageNumberLimit,
    itemsPerPage,
    indexOfFirstItem,
    indexOfLastitem,
    onTrigger,
    handleClick,
    handleNextbtn,
    handlePrevbtn,
    renderPageNumbers,
  } = RecommendCarouselHook(data);

  return (
    <div>
      <>
        <div>
          <ul>
            <li>
              <button className="gauche2" onClick={handlePrevbtn}>
              &lt;
              </button>
            </li>
            <li>
              <button className="droite2" onClick={handleNextbtn}>
              &gt;
              </button>
            </li>
          </ul>
        </div>
      </>
    </div>
  );
};

export default RecommendCarousel;
