import React, { useEffect, useState } from "react";
import "react-dropdown/style.css";
import "../../assets/styles/Search.scss";

import BreadCrumb from "../breadCrumb/BreadCrumb";
import Aside from "../sidebar/AsideComponent";
import useSearchHook from "./useSearchHook";
import ListOfProducts from "../product/ListOfProducts";
import Pagination from "../pagination/Pagination";
import Section from "../genericComponents/Section";
const ViewSearch = (props) => {
  const [empty, setEmpty] = useState(false);
  const {
    products,
    paginatedData,
    filteredData,
    filterProducts,
    productsToList,
    pages,
    filterOn,
    filters,
    isLoaded,
    searchTerm,
    breadCrumbNumber,
  } = useSearchHook();

  return (
      <div className="TopVentes">
          {/* <h3>
            <BreadCrumb
              viewName={"Rayons"}
              length={breadCrumbNumber}
              searchTerm={searchTerm}
              parentFilter={filterProducts}
            />
          </h3> */}
          <Section section={"Rayons"} />
          <Aside parentFilter={filterProducts} />
          <div>
            <ListOfProducts
              // parentCallback={handleCallback}
              toLoop={paginatedData}
              searchTerm={searchTerm}
              products={paginatedData}
              noItems={paginatedData.length === 0 ? true : false}
            />
            {/* {products.length ? ( */}
            {/* {empty === true ? (
              <Pagination
                parentCallback={productsToList}
                pages={[0]}
                empty={true}
                products={[]}
              />
            ) : ( */}
            <Pagination
              parentCallback={productsToList}
              products={filterOn ? filteredData : products}
              resetCurrentPage={filterOn ? true : false}
              nofilter={products.length == 0 ? true : false}
              filterOn={filterOn}
              pages={pages}
            />
            {/* )} */}
            {/* ) : ( */}
            {/* <span>loading ..</span> */}
            {/* )} */}
          </div>
        </div>
  );
};

export default ViewSearch;
