import React from "react";
import { Link } from "react-router-dom";
import { useEffect, useState } from "react";
import Rien from "../../assets/images/rien.jpg";
const Panier = (props) => {
  let panier = JSON.parse(localStorage.getItem("Panier")) || [];

  const [products, setProducts] = React.useState(panier);
  const [res, setRes] = useState(0);
  const [isStoring, setIsStoring] = useState();
  const [listOfShop, setListOfShop] = useState();

  let itemCount = panier.length;
  let sous_total = 0;
  let rounded = 0;
  let menu = "0";
  let total = 0;
  const valeurlivraison = (value) => {
    setRes(menu.value);
  };
  const onChangeProductQuantity = (index, event) => {
    let cartItem = panier[index];
    if (event.target.value > cartItem.quantity) {
      cartItem.quantity++;
    } else {
      cartItem.quantity--;
    }
    setListOfShop(panier);
    setIsStoring(true);
  };

  const onRemoveProduct = (i) => {
    const filteredProduct = products.filter((product, index) => {
      return index != i;
    });
    localStorage.setItem("Panier", JSON.stringify(filteredProduct));
    setProducts(filteredProduct);
  };

  useEffect(() => {
    if (isStoring == true) {
      localStorage.setItem("Panier", JSON.stringify(listOfShop));
      setIsStoring(!isStoring);
    }
  }, [isStoring]);

  return (
    <div className=" containerShopping">
      <div className="leftpan">
        <div className="toppan">
          <h1 className="panierpop">MON PANIER</h1>
          
        </div>
        <h2 className="h2popgauch">
            il y a {itemCount} produits dans votre panier
          </h2>
        <div>
          {panier.map((product, index) => {
            let livre = parseFloat(res);
            sous_total = sous_total + product.product.price * product.quantity;
            rounded = Math.round((sous_total + Number.EPSILON) * 100) / 100;
            total = rounded + livre;

            return (
              <li key={index}>
                <div className="unitempan">
                  {product.product.image ? (
                    <Link to={`/product/${product.product._id}`}>
                      <img
                        className="imgpan"
                        src={product.product.image}
                        alt={product.product.name}
                      />
                    </Link>
                  ) : (
                    <Link to={`/product/${product.product._id}`}>
                      <img src={Rien} className="imgpan" />
                    </Link>
                  )}

                  <div>
                    <Link to={`/product/${product.product._id}`}>
                      <p className="agrandpan">{product.product.name}</p>
                    </Link>
                    <p className="prixpan">
                      {product.product.price}€
                    </p>
                    <p className="descrpan" id="style-1">Description : <br/><span className="decrmargpan">{product.product.description}</span></p>
                    
                    <input
                      onChange={(event) =>
                        onChangeProductQuantity(index, event)
                      }
                      type="number"
                      className="nbrarticlepan"
                      defaultValue={product.quantity}
                    />
                    <button type="submit" className="btnaddpan">
                      <span>Rajouté un produit</span>
                    </button>
                  </div>
                  <div>
                    <svg
                      onClick={() => onRemoveProduct(index)}
                      version="1.1"
                      className="removepan"
                      x="0px"
                      y="0px"
                      viewBox="0 0 60 60"
                      enableBackground="new 0 0 60 60"
                    >
                      <polygon points="38.936,23.561 36.814,21.439 30.562,27.691 24.311,21.439 22.189,23.561 28.441,29.812 22.189,36.064 24.311,38.186 30.562,31.934 36.814,38.186 38.936,36.064 32.684,29.812" />
                    </svg>
                  </div>
                </div>
              </li>
            );
          })}
        </div>
      </div>
      <div className="rightpan">
        <h2 className="gauche_prix">{total}€</h2>
        <h2>Total</h2>
        <h3 className="gauche_prix">{rounded}€</h3>
        <h3>Sous Total</h3>
        <h3 className="gauche_prix">{res}€</h3>
        <h3>Livraison</h3>
        <form className="typelivraison">
          <select
            ref={(input) => (menu = input)}
            onChange={(event) => valeurlivraison(menu, event)}
          >
            <option value="0">Livraison Standard 7/14jours</option>
            <option value="2">Livraison rapide 2/7jours + 2 €</option>
            <option value="5">Livraison en 24h + 5 €</option>
          </select>
          <button type="submit" className="btnpan">
            <span>Commander</span>
          </button>
        </form>
      </div>
    </div>
  );
};

export default Panier;
