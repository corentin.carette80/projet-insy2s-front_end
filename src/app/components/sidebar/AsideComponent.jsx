import React, { useEffect, useState } from "react";
import Dropdown from "react-dropdown";
import "react-dropdown/style.css";
import "../../assets/styles/AsideComponent.scss";

const ProductList = (props) => {
  const [category, setCategory] = useState(null);
  const [subCategory, setSubCategory] = useState(null);
  const [price, setPrice] = useState("0.10000");
  const [sort, setSort] = useState(null);
  const [filter, setFilter] = useState(null);
  const [selected, setSelected] = React.useState("");
  const setFilterOptions = (event, setter, stateParam) => {
    setter(event.target.value);
    if (stateParam === "category") {
      setSelected(event.target.value);
      setFilter({
        category: event.target.value,
        subCategory: subCategory,
        price: price,
        sort: sort,
      });
    } else if (stateParam === "subCategory") {
      setFilter({
        category: category,
        subCategory: event.target.value,
        price: price,
        sort: sort,
      });
    } else if (stateParam === "price") {
      setFilter({
        category: category,
        subCategory: subCategory,
        price: event.target.value,
        sort: sort,
      });
    } else if (stateParam === "sort") {
      setFilter({
        category: category,
        subCategory: subCategory,
        price: price,
        sort: event.target.value,
      });
    }
  };
  useEffect(() => {
    if (filter !== null) {
      props.parentFilter(filter);
    }
  }, [filter]);

  // const setFilterSort = (event) => {
  //   setSort(event.target.value);
  //   props.parentFilter([category, subCategory, price, event.target.value]);
  // };
  // const changeSelectOption = (e, setFieldValue) => {

  //   const selectedId = e.target.value;
  //   const selectedSubcategory = categories.find((d) => d._id === selectedId);
  //   setSelected(selectedSubcategory.subCategories);
  //   setFieldValue("category", selectedId);
  // };

  const options = [
    {
      type: "group",
      name: "Catégories",
      items: [
        { value: "films", label: "Films" },
        { value: "series", label: "Séries" },
        { value: "goodies", label: "Goodies" },
      ],
    },
  ];

  const optionsSubCategories = [
    {
      type: "group",
      name: "Sous-catégories",
      items: [
        { value: "selection", label: "Notre sélection" },
        {
          value: "action",
          label: "Actions & Aventures",
        },
        { value: "crime", label: "Crime" },
        {
          value: "documentaire",
          label: "Documentaire",
        },
        {
          value: "fantastique",
          label: "Fantastique",
        },
        {
          value: "sciencefiction",
          label: "Sci-Fi",
        },
        {
          value: "comedie",
          label: "Comédie",
        },
        { value: "drame", label: "Drama" },
        {
          value: "horreur",
          label: "Horreur",
        },
        {
          value: "animation",
          label: "Animation",
        },
      ],
    },
  ];

  const optionsGoodies = [
    {
      type: "group",
      name: "GOODIES",
      items: [
        {
          value: "epicerie",
          label: "Epicerie",
        },
        {
          value: "vetements",
          label: "Vêtements",
        },
        {
          value: "maison",
          label: "Maison",
        },
        {
          value: "jeux",
          label: "Jeux/Jouets",
        },
      ],
    },
  ];

  const optionsPrice = [
    {
      type: "group",
      name: "PRICES",
      items: [
        {
          value: "0.10000",
          label: "Tous",
        },
        {
          value: "0.100",
          label: "0 - 100 €",
        },
        {
          value: "100.200",
          label: "100 - 200 €",
        },
        {
          value: "200.10000",
          label: "200 € et +",
        },
      ],
    },
  ];

  const optionsSort = [
    {
      type: "group",
      name: "SORT",
      items: [
        {
          value: "AZ",
          label: "A - Z",
        },
        {
          value: "ZA",
          label: "Z - A",
        },
        {
          value: "09",
          label: "Croissant",
        },
        {
          value: "90",
          label: "Décroissant",
        },
      ],
    },
  ];
  /** Type variable to store different array for different dropdown */
  let type = null;

  /** This will be used to create set of options that user will see */
  let optionsOfSelectInput = null;

  /** Setting Type variable according to dropdown */
  console.log(selected);
  if (selected === "films") {
    type = optionsSubCategories;
  } else if (selected === "séries") {
    type = optionsSubCategories;
  } else if (selected === "goodies") {
    type = optionsGoodies;
  }
  /** If "Type" is null or undefined then options will be null,
   * otherwise it will create a options iterable based on our array
   */
  if (type) {
    optionsOfSelectInput = type[0].items.map((el, index) => (
      <option value={el.value} key={index}>
        {el.label}
      </option>
    ));
  }
  // const sendRequestCategory = (e) => {
  //   console.log("catégorie:", e.target.value);
  //   props.parentFilter(e.target.value, subCategory);
  // };
  // const sendRequestSubCategory = (e) => {
  //   console.log("sous catégorie:", e.target.value);
  //   setSubCategory(e.target.value);
  //   // setFilter(e.target.value);
  //   console.log("c, sc", category, e.target.value);
  //   props.parentFilter(category, e.target.value);
  // };

  return (
    // <div className="contains">
    <div className="filtersProducts">
      <div className="filtre">
        <select
          onChange={(e) => setFilterOptions(e, setCategory, "category")}
          name=""
          id=""
        >
          <option selected disabled>
            Catégorie
          </option>
          {options[0].items.map((value, index) => {
            console.log(value);
            return (
              <>
                <option key={index} value={value.value}>
                  {value.label}
                </option>
              </>
            );
          })}
        </select>
        <select
          onChange={(e) => setFilterOptions(e, setSubCategory, "subCategory")}
          name=""
          id=""
        >
          <option disabled selected>
            Sous Catégorie
          </option>
          {optionsOfSelectInput}
        </select>
        <select
          onChange={(e) => setFilterOptions(e, setPrice, "price")}
          name=""
          id=""
        >
          <option selected disabled>
            Prix
          </option>
          {optionsPrice[0].items.map((value, index) => {
            return (
              <>
                <option key={index} value={value.value}>
                  {value.label}
                </option>
              </>
            );
          })}
        </select>
      </div>
      <div className="sortFilter">
        <select onChange={(e) => setFilterOptions(e, setSort, "sort")}>
          <option selected disabled>
            Trier par
          </option>
          {optionsSort[0].items.map((value, index) => {
            return (
              <>
                <option key={index} value={value.value}>
                  {value.label}
                </option>
              </>
            );
          })}
        </select>
      </div>
    </div>
  );
};

export default ProductList;
