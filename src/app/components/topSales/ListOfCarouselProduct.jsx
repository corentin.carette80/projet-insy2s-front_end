import React from "react";
import { Link } from "react-router-dom";
import Vector from "../../assets/images/Vector.png";
import Panier2 from "../../assets/images/panier2.png";
import Rien from "../../assets/images/rien.jpg";
import Coeur from "../../assets/images/Coeur.png";
import Favorites from "../Favorites/Favorites";
import Buy from "../Buy/Buy";
import Section from "../genericComponents/Section";
import ProductSheet from "../genericComponents/ProductSheet";

const ListOfCarouselProduct = (products) => {
  return (
    <div className="carouss_container">
      <Section section={"Vous pourriez aimer aussi"} />

        <div className="articles esp_top">
          {products.products.map((product) => (
            <ProductSheet
            product={product}
            title={product.name}
            image={product.image ? product.image : Rien}
            price={product.price}
            id={product._id}
          />
          ))}
        </div>
      </div>
  );
};

export default ListOfCarouselProduct;
