import React from "react";
import { useEffect, useState } from "react";
import axios from "axios";

const useTopSalesHook = () => {
  const URLProducts = "http://localhost:5000/api/products/";
  const [products, setProducts] = useState([]);
  const [paginatedData, setPaginatedData] = useState([]);
  const [isLoaded, setIsLoaded] = useState(false);
  const [filters, setFilters] = useState([]);
  const [filterOn, setFilterOn] = useState(false);
  const [filteredData, setFilteredData] = useState();
  const [pages, setPages] = useState([]);
  const [noFilter, setNoFilter] = useState();

  useEffect(() => {
    axios.get(URLProducts).then((response) => {
      setIsLoaded(!isLoaded);
      setProducts(response.data.products);
      setFilteredData(response.data.products);
    });
    const tableau = [];
    for (let i = 1; i <= Math.ceil(products?.length / 5); i++) {
      tableau.push(i);
    }
    setPages([...tableau]);
  }, []);

  const productsToList = (childData) => {
    console.log(childData);
    setPaginatedData(childData);
  };

  console.log(products);

  const filterProducts = (childData) => {
    console.log(childData);

    let filtre;
    let DataFiltered = products;
    let filterCategory;
    let filterPrice;
    let filterFinal;
    let rangeMaxPrice;
    let rangeMinPrice;

    // if (
    //   childData.category === null &&
    //   childData.subCategory === null &&
    //   childData.price === null &&
    //   childData.sort === null
    // ) {
    //   setNoFilter(true);
    // }

    // FILTRE EN FONCTION CATEGORIE / SOUS CAT. //
    if (childData.subCategory !== null) {
      // setFilters([childData.category, childData.subCategory]);
      filterCategory = products?.filter((item) => {
        return (
          item.categoryId[0].name.toLowerCase().includes(childData.category) &&
          item.subCategoryId[0].name
            .toLowerCase()
            .includes(childData.subCategory)
        );
      });
    } else if (childData.subCategory === null && childData.category === null) {
      console.log("pas sous cat et cat");
      filterCategory = products;
    } else {
      // setFilters([childData.category]);
      filterCategory = products?.filter((item) => {
        return item.categoryId[0].name
          .toLowerCase()
          .includes(childData.category);
      });
    }
    // FILTRE EN FONCTION CATEGORIE / SOUS CAT. //

    // if (childData.price !== null) {
    rangeMinPrice = childData.price.split(".")[0];
    rangeMaxPrice = childData.price.split(".")[1];
    console.log(filterCategory);
    filterPrice = filterCategory?.filter((item) => {
      return item.price >= rangeMinPrice && item.price <= rangeMaxPrice;
    });
    // }

    // if (childData.category === null && childData.subCategory === null) {
    //   DataFiltered = products;
    // }
    // if (
    //   childData.category === null &&
    //   childData.subCategory === null &&
    //   childData.price === "*"
    // ) {
    //   DataFiltered = products;
    // }

    // if (childData.sort !== "null") {
    // console.log(childSortFilter);
    if (childData.sort === "AZ") {
      console.log(filterPrice);
      const sortMethod = (a, b) => {
        if (a.name.toLowerCase() < b.name.toLowerCase()) {
          return -1;
        }
        if (a.name.toLowerCase() > b.name.toLowerCase()) {
          return 1;
        }
        return 0;
      };
      filterFinal = filterPrice?.sort(sortMethod);
    } else if (childData.sort === "ZA") {
      console.log(filterPrice);
      const sortMethod = (a, b) => {
        if (b.name.toLowerCase() < a.name.toLowerCase()) {
          return -1;
        }
        if (b.name.toLowerCase() > a.name.toLowerCase()) {
          return 1;
        }
        return 0;
      };
      filterFinal = filterPrice?.sort(sortMethod);
    } else if (childData.sort === "09") {
      console.log(filterPrice);
      const sortMethod = (a, b) => {
        if (a.price < b.price) {
          return -1;
        }
        if (a.price > b.price) {
          return 1;
        }
        return 0;
      };
      filterFinal = filterPrice?.sort(sortMethod);
    } else if (childData.sort === "90") {
      console.log(filterPrice);
      const sortMethod = (a, b) => {
        if (b.price < a.price) {
          return -1;
        }
        if (b.price > a.price) {
          return 1;
        }
        return 0;
      };
      filterFinal = filterPrice?.sort(sortMethod);
    } else {
      filterFinal = filterPrice;
    }
    // }

    setFilteredData(filterFinal);
    console.log("filteredData :", filteredData);
    setFilterOn(true);
    const tableau = [];
    for (let i = 1; i <= Math.ceil(Object.keys(filterFinal)?.length / 5); i++) {
      tableau.push(i);
    }
    setPages([...tableau]);
  };

  useEffect(() => {
    const tableau = [];
    for (let i = 1; i <= Math.ceil(products?.length / 5); i++) {
      tableau.push(i);
    }
    setPages([...tableau]);
  }, [products]);
  return {
    products,
    paginatedData,
    filteredData,
    filterOn,
    filters,
    filterProducts,
    productsToList,
    isLoaded,
    pages,
  };
};

export default useTopSalesHook;
