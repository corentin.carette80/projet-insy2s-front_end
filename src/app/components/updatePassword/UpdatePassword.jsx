import { Field, Form, Formik } from "formik";
import React, { useState } from "react";
import "../../assets/styles/profile.scss";
import Section from "../genericComponents/Section";
import useUpdatePasswordHook from "./useUpdatePasswordHook";
const UpdatePassword = () => {
  const [convertisseur, setConvertisseur] = useState(false);
  const { onSubmit, validate, email, password, userEmail } =
    useUpdatePasswordHook();
  const changeInput = (e) => {
    setConvertisseur(!convertisseur);
  };
  return (
    <div className="mdp_container">
      <Section section={"Modifier mon mot de passe"} />

      <Formik
        initialValues={{
          PasswordNow: "",
          NewPassword: "",
          ConfirmPassword: "",
        }}
        validationSchema={validate}
        onSubmit={onSubmit}
      >
        {(formik) => (
          <Form className="esp_information">
            <label  htmlFor="PasswordNow">
              <p>Mot de passe actuels</p>
            </label>
            <Field
              name="PasswordNow"
              type={convertisseur ? "text" : "password"}
              placeholder="veuillez saisir votre mot de passe "
              
            />
            <label  htmlFor="NewPassword">
              Nouveau mot de passe
            </label>
            <Field
              name="NewPassword"
              type={convertisseur ? "text" : "password"}
              placeholder="veuillez saisir votre nouveaux mot de passe "
              
            />
            <label htmlFor="ConfirmPassword">
              Confirmer le mot de passe
            </label>
            <Field
              name="ConfirmPassword"
              type={convertisseur ? "text" : "password"}
              placeholder="veuillez confirmer votre mot de passe "
            />
            <input
              type="checkbox"
              value="text"
              onClick={changeInput}
            />
            <label className="invi_inf"> 
          <input
            type="checkbox"
            defaultValue="text"
            onClick={changeInput}
          /><span className="trymdp"><i className="fa fa-search see_inf"></i></span>
          </label>
            <br />
            <Field
            name="subm"
            type="submit"
            defaultValue="Enregistrer"
          />
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default UpdatePassword;
