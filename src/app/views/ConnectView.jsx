import React from "react";
import "../assets/styles/connexion.scss";
import ConnectView from "../components/connect/ConnectView";

const ConnexionView = (props) => {
  return (
    <>
      <ConnectView {...props} />
    </>
  );
};

export default ConnexionView;
