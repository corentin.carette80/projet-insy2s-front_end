import React from "react";
import Adresse from "../components/profile/Adresse";
import Information from "../components/profile/Information";
const information = () => {
  return (
    <div>
      <Information />
      <Adresse/>
    </div>
  );
};

export default information;
