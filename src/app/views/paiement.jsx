import React from 'react';
import Section from '../components/genericComponents/Section';
import Navbarprofile from "../components/profile/Navbarprofile";
import { Field, Form, Formik } from "formik";
const paiement = () => {
    return (
        <div className="flex">
        <Navbarprofile/>
        <div className="addproduct_container">
                <Section section={"Ajouter une carte"} />
                <Formik>
                <Form>
                    <label>
                      Nom
                    </label>
                      <Field
                        type="text"
                      />
                    <label>
                      Prénom
                    </label>
                      <Field
                        type="text"
                      />
                    <label>
                      Numéro
                    </label>
                      <Field
                        type="number"
                        maxLength="11"
                      />
                    <label>
                      Date D'expiration
                    </label>
                      <Field
                        type="date"
                      />
                      <label>
                      Code CVV
                    </label>
                      <Field
                        type="number"
                      />
                  <button
                    className="button_add_product"
                    type="submit"
                  >
                    ajouter paiement
                  </button>
                </Form>
                </Formik>
              </div>
    </div>
    );
};

export default paiement;