import React, { useState } from "react";
import Navbarprofile from "../components/profile/Navbarprofile";

const ProfileView = (props) => {
  return (
    <div className="flex">
      <Navbarprofile />
      {props.section}
    </div>
  );
};

export default ProfileView;
